let number = Number(prompt("Enter a number:"));
console.log(`The number you provided is ${number}.`);

for (let i = number; i >= 0; i--) {
	if (i <= 50) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	};

	if (i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	};

	if (i % 5 === 0) {
		console.log(i);
	};
};

let activityString = "supercalifragilisticexpialidocious";
let stringConsonant = "";

for (let i = 0; i < activityString.length; i++) {
	if(
		activityString[i] == "a" ||
		activityString[i] == "e" ||
		activityString[i] == "i" ||
		activityString[i] == "o" ||
		activityString[i] == "u"
	) {
		continue;
	} else {
		stringConsonant += activityString[i];
	};
};

console.log(activityString);
console.log(stringConsonant);